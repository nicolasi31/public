**Alpine Linux Tips and Tricks**

[TOC]

# Package Owner
```shell
apk info --who-owns /usr/sbin/dnsd
```

# Package Content
```shell
apk info -L bash
```
