|interface|state|type|mac|bandwidth|speed|duplex|mode|mtu|description|ip|mask| 
|---|---|---|---|---|---|---|---|---|---|---|---|
|Ethernet1/1|up|100/1000/10000 Ethernet|00bb.2cfc.0101|1000000|1000 Mb/s|full|trunk|1500|null|null|null|
|Ethernet1/10|down|100/1000/10000 Ethernet|00bb.2cfc.010a|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/11|down|100/1000/10000 Ethernet|00bb.2cfc.010b|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/12|down|100/1000/10000 Ethernet|00bb.2cfc.010c|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/13|down|100/1000/10000 Ethernet|00bb.2cfc.010d|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/14|down|100/1000/10000 Ethernet|00bb.2cfc.010e|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/15|down|100/1000/10000 Ethernet|00bb.2cfc.010f|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/16|down|100/1000/10000 Ethernet|00bb.2cfc.0110|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/17|down|100/1000/10000 Ethernet|00bb.2cfc.0111|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/18|down|100/1000/10000 Ethernet|00bb.2cfc.0112|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/19|down|100/1000/10000 Ethernet|00bb.2cfc.0113|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/2|up|100/1000/10000 Ethernet|00bb.2cfc.0102|1000000|1000 Mb/s|full|trunk|1500|null|null|null|
|Ethernet1/20|down|100/1000/10000 Ethernet|00bb.2cfc.0114|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/21|down|100/1000/10000 Ethernet|00bb.2cfc.0115|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/22|down|100/1000/10000 Ethernet|00bb.2cfc.0116|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/23|down|100/1000/10000 Ethernet|00bb.2cfc.0117|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/24|down|100/1000/10000 Ethernet|00bb.2cfc.0118|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/25|down|100/1000/10000 Ethernet|00bb.2cfc.0119|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/26|down|100/1000/10000 Ethernet|00bb.2cfc.011a|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/27|down|100/1000/10000 Ethernet|00bb.2cfc.011b|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/28|down|100/1000/10000 Ethernet|00bb.2cfc.011c|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/29|down|100/1000/10000 Ethernet|00bb.2cfc.011d|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/3|up|100/1000/10000 Ethernet|00bb.2cfc.0103|1000000|1000 Mb/s|full|access|1500|null|null|null|
|Ethernet1/30|down|100/1000/10000 Ethernet|00bb.2cfc.011e|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/31|down|100/1000/10000 Ethernet|00bb.2cfc.011f|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/32|down|100/1000/10000 Ethernet|00bb.2cfc.0120|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/33|down|100/1000/10000 Ethernet|00bb.2cfc.0121|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/34|down|100/1000/10000 Ethernet|00bb.2cfc.0122|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/35|down|100/1000/10000 Ethernet|00bb.2cfc.0123|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/36|down|100/1000/10000 Ethernet|00bb.2cfc.0124|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/37|down|100/1000/10000 Ethernet|00bb.2cfc.0125|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/38|down|100/1000/10000 Ethernet|00bb.2cfc.0126|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/39|down|100/1000/10000 Ethernet|00bb.2cfc.0127|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/4|up|100/1000/10000 Ethernet|00bb.2cfc.0104|1000000|1000 Mb/s|full|access|1500|null|null|null|
|Ethernet1/40|down|100/1000/10000 Ethernet|00bb.2cfc.0128|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/41|down|100/1000/10000 Ethernet|00bb.2cfc.0129|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/42|down|100/1000/10000 Ethernet|00bb.2cfc.012a|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/43|down|100/1000/10000 Ethernet|00bb.2cfc.012b|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/44|down|100/1000/10000 Ethernet|00bb.2cfc.012c|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/45|down|100/1000/10000 Ethernet|00bb.2cfc.012d|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/46|down|100/1000/10000 Ethernet|00bb.2cfc.012e|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/47|down|100/1000/10000 Ethernet|00bb.2cfc.012f|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/48|down|100/1000/10000 Ethernet|00bb.2cfc.0130|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/49|down|100/1000/10000 Ethernet|00bb.2cfc.0131|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/5|down|100/1000/10000 Ethernet|00bb.2cfc.1b08|10000000|auto-speed|auto|null|1500|L3 Link|172.16.1.1|30|
|Ethernet1/50|down|100/1000/10000 Ethernet|00bb.2cfc.0132|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/51|down|100/1000/10000 Ethernet|00bb.2cfc.0133|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/52|down|100/1000/10000 Ethernet|00bb.2cfc.0134|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/53|down|100/1000/10000 Ethernet|00bb.2cfc.0135|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/54|down|100/1000/10000 Ethernet|00bb.2cfc.0136|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/55|down|100/1000/10000 Ethernet|00bb.2cfc.0137|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/56|down|100/1000/10000 Ethernet|00bb.2cfc.0138|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/57|down|100/1000/10000 Ethernet|00bb.2cfc.0139|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/58|down|100/1000/10000 Ethernet|00bb.2cfc.013a|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/59|down|100/1000/10000 Ethernet|00bb.2cfc.013b|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/6|down|100/1000/10000 Ethernet|00bb.2cfc.0106|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/60|down|100/1000/10000 Ethernet|00bb.2cfc.013c|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/61|down|100/1000/10000 Ethernet|00bb.2cfc.013d|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/62|down|100/1000/10000 Ethernet|00bb.2cfc.013e|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/63|down|100/1000/10000 Ethernet|00bb.2cfc.013f|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/64|down|100/1000/10000 Ethernet|00bb.2cfc.0140|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/7|down|100/1000/10000 Ethernet|00bb.2cfc.0107|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/8|down|100/1000/10000 Ethernet|00bb.2cfc.0108|10000000|auto-speed|auto|access|1500|null|null|null|
|Ethernet1/9|down|100/1000/10000 Ethernet|00bb.2cfc.0109|10000000|auto-speed|auto|access|1500|null|null|null|
|Vlan1|null|null|null|null|null|null|null|null|null|null|null|
|Vlan100|null|null|null|null|null|null|null|null|null|172.16.100.1|24|
|Vlan101|null|null|null|null|null|null|null|null|null|172.16.101.1|24|
|Vlan102|null|null|null|null|null|null|null|null|null|172.16.102.1|24|
|Vlan103|null|null|null|null|null|null|null|null|null|172.16.103.1|24|
|Vlan104|null|null|null|null|null|null|null|null|null|172.16.104.1|24|
|Vlan105|null|null|null|null|null|null|null|null|null|172.16.105.1|24|
|loopback1|up|Loopback|null|8000000|null|null|null|1500|null|172.16.0.1|32|
|loopback30|up|Loopback|null|8000000|null|null|null|1500|My Learning Lab Loopback|null|null|
|loopback98|up|Loopback|null|8000000|null|null|null|1500|Configured using OpenConfig Model|10.98.98.1|24|
|loopback99|up|Loopback|null|8000000|null|null|null|1500|Full intf config via NETCONF|10.99.99.1|24|
|mgmt0|up|Ethernet|0050.56bb.2cfc|1000000|1000 Mb/s|full|null|1500|DO NOT TOUCH CONFIG ON THIS INTERFACE|10.10.20.95|24|
|port-channel11|up|Port-Channel|00bb.2cfc.0101|2000000|1000 Mb/s|full|trunk|1500|null|null|null|
