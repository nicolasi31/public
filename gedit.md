**Gedit Tips and Tricks**

[TOC]

# Column selection
- Install package gedit-plugin-multi-edit
- Type Ctrl-Shift-C
- Select text
- Type Return key
- Replace or Delete selection
- Type Ctrl-Shift-C

